jQuery(document).ready(function($) {
  (function($){$.belowthefold=function(element,settings){var fold=$(window).height()+$(window).scrollTop();return fold<=$(element).offset().top-settings.threshold;};$.abovethetop=function(element,settings){var top=$(window).scrollTop();return top>=$(element).offset().top+$(element).height()-settings.threshold;};$.rightofscreen=function(element,settings){var fold=$(window).width()+$(window).scrollLeft();return fold<=$(element).offset().left-settings.threshold;};$.leftofscreen=function(element,settings){var left=$(window).scrollLeft();return left>=$(element).offset().left+$(element).width()-settings.threshold;};$.inviewport=function(element,settings){return!$.rightofscreen(element,settings)&&!$.leftofscreen(element,settings)&&!$.belowthefold(element,settings)&&!$.abovethetop(element,settings);};$.extend($.expr[':'],{"below-the-fold":function(a,i,m){return $.belowthefold(a,{threshold:0});},"above-the-top":function(a,i,m){return $.abovethetop(a,{threshold:0});},"left-of-screen":function(a,i,m){return $.leftofscreen(a,{threshold:0});},"right-of-screen":function(a,i,m){return $.rightofscreen(a,{threshold:0});},"in-viewport":function(a,i,m){return $.inviewport(a,{threshold:0});}});})(jQuery);
  var upto15 = [{'est':118300, 'int1':103000, 'int2':133700},{'est':150800, 'int1':125100, 'int2':176600},{'est':202500, 'int1':171700, 'int2':233300},{'est':165200, 'int1':139600, 'int2':190800},{'est':155400, 'int1':132000, 'int2':178900},{'est':161100, 'int1':135900, 'int2':186200},{'est':152400, 'int1':129700, 'int2':175100},{'est':165100, 'int1':139900, 'int2':190200},{'est':170100, 'int1':144600, 'int2':195700},{'est':172700, 'int1':146800, 'int2':198600},{'est':185900, 'int1':156600, 'int2':215100},{'est':181500, 'int1':152400, 'int2':210500},{'est':193200, 'int1':162500, 'int2':223900},{'est':192000, 'int1':161400, 'int2':222600},{'est':188400, 'int1':158100, 'int2':218700}];
  var upto12 = [{'est':114600, 'int1':99600, 'int2':129500},{'est':142600, 'int1':118500, 'int2':166700},{'est':190000, 'int1':160600, 'int2':219400},{'est':156100, 'int1':131900, 'int2':180200},{'est':146300, 'int1':124300, 'int2':168400},{'est':152200, 'int1':128500, 'int2':176000},{'est':144500, 'int1':122500, 'int2':166600},{'est':158400, 'int1':134300, 'int2':182600},{'est':161200, 'int1':136900, 'int2':185500},{'est':164400, 'int1':139400, 'int2':189300},{'est':177800, 'int1':149800, 'int2':205800},{'est':172000, 'int1':144400, 'int2':199500},{'est':184100, 'int1':154400, 'int2':213800},{'est':181600, 'int1':152300, 'int2':210900},{'est':178100, 'int1':149000, 'int2':207100}];
  var less5 = [{'est':69600, 'int1':60000, 'int2':79200},{'est':70900, 'int1':60200, 'int2':81600},{'est':77100, 'int1':65600, 'int2':88600},{'est':72400, 'int1':59900, 'int2':84800},{'est':71200, 'int1':59500, 'int2':82800},{'est':72800, 'int1':61300, 'int2':84300},{'est':72800, 'int1':61800, 'int2':83800},{'est':78400, 'int1':66500, 'int2':90300},{'est':80200, 'int1':67700, 'int2':92600},{'est':82300, 'int1':69200, 'int2':95400},{'est':90600, 'int1':76100, 'int2':105100},{'est':89200, 'int1':74000, 'int2':104500},{'est':92200, 'int1':74900, 'int2':109600},{'est':89500, 'int1':73300, 'int2':105800},{'est':83700, 'int1':68200, 'int2':99300}];
  var all = [{'est':152600, 'int1':134400, 'int2':170800},{'est':191000, 'int1':161500, 'int2':220500},{'est':255100, 'int1':221100, 'int2':289100},{'est':212400, 'int1':182800, 'int2':242100},{'est':206700, 'int1':177500, 'int2':235900},{'est':210300, 'int1':179800, 'int2':240700},{'est':202300, 'int1':175100, 'int2':229500},{'est':220500, 'int1':190300, 'int2':250800},{'est':232900, 'int1':200000, 'int2':265700},{'est':235300, 'int1':202400, 'int2':268200},{'est':250100, 'int1':214100, 'int2':286000},{'est':251700, 'int1':216100, 'int2':287200},{'est':262300, 'int1':225400, 'int2':299200},{'est':265000, 'int1':228600, 'int2':301300},{'est':256700, 'int1':220600, 'int2':292900}];

  var linetip = {
    active : false,
    follow : function(index){
      d3.selectAll('.boxes .tooltip')
        .transition().duration(100)
        .attr('transform', function(d,i){
          return 'translate(0,'+(y(d.about[index].est) - (height/contents.length))+')';
        });
    },
    stay : function(index){
      d3.selectAll('.boxes .tooltip')
        .transition().duration(100)
        .attr('transform', function(d,i){
          var hg = height / (contents.length - 1);
          return 'translate(0,'+(height - (hg * (i + 1)))+')';
        });
    }
  },
  axisDraw = {
    active : true,
    axisRange : false,
    draw : function(that){
      if(that.about === '')
        y.domain([0, d3.max(all, function(d) { return d.int2; })]);
      else
        this.pickDomain(that);

      d3.selectAll(".line").transition().duration(2000).attr("d", line);
      d3.selectAll(".area").transition().duration(2000).attr("d", area);

      d3.select('.y.axis')
        .transition()
        .duration(1000)
        .call(yAxis); 
        this.resetFlag();    
    },
    resetFlag : function(){
      for (var i = 0; i < all.length; i++) {
        if(linetip.active)
          linetip.follow(i);
        else
          linetip.stay();
      };
    },
    pickDomain : function(that){
      if(this.axisRange){
        y.domain([d3.min(that.about, function(d,i) { return d.int1; }), d3.max(that.about, function(d,i) { return d.int2; }) * 1.1]); 
      }else{
        y.domain([0, d3.max(that.about, function(d,i) { return d.int2; }) * 1.1]);
      }      
    }
  };
  var mean = d3.mean(all,function(d) { return +d.est});
  var started = false;
  var margin = {top: 30, right: 80, bottom: 70, left: 100},
      h = 500, w = 1050, yaxisp = -50,
      width = w - margin.left - margin.right,
      height = h - margin.top - margin.bottom;

  var contents = [{'about':less5,'cls':'less5','title':'Less Than 5 Years Old',
                  'quote':'Children younger than 5 years of age, especially those under the age of 3, are most at risk for choking on small toys and small parts of toys.  Many emergency room visits and deaths are related to airway obstruction. Rate of injury increase from 1999-2013 was 20%.',
                  draw:function(){
                    drawAxis(this);
                  },
                  undraw:function(){
                    undrawAxis(this);
                  }},
                  {'about':upto12,'cls':'upto12','title':'Less Than 12',
                  'quote':'As children get older, injuries involving riding toys increase on devices like scooters and tricycles. These types of injuries are commonly associated with broken bones and dislocations. The spike you see in the year 2000 is directly related to the rise and popularity of scooters.',
                  draw:function(){
                    drawAxis(this);
                  },
                  undraw:function(){
                    undrawAxis(this);
                  }},
                  {'about':upto15,'cls':'upto15','title':'Less Than 15',
                  'quote':'From 1999 to 2013, injuries requiring hospitalization increased by 59%. Riding toys are especially dangerous for kids in this age group. For the year 2013 riding toys also accounted for 38% of injuries, higher than any other category of toy. Non-motorized scooters accounted for 73% of the estimated injuries related to riding toys.',
                  draw:function(){
                    drawAxis(this);
                  },
                  undraw:function(){
                    undrawAxis(this);
                  }},
                  {'about':all,'cls':'all','title':'All Ages',
                  'quote':'For each age group, there is a spike in total injuries around 2000-2001. These increases are attributed primarily to rises in popularity of non-motorized scooters at that time. Riding toys are the #1 cause of hospitalizations each year.',
                  draw:function(){
                    drawAxis(this);
                  },
                  undraw:function(){
                    undrawAxis(this);
                  }},
                  {'about':'','cls':'allOfThem','title':'All Of Them',
                  'quote':'For Children younger than 5 years of age, toy vehicles, nonmotorized scooters, ',
                  draw:function(){
                    drawAxis(this);
                  },
                  undraw:function(){
                    allUndraw();
                  }}];

  var x = d3.time.scale()
      .range([0, width]);

  var y = d3.scale.linear()
      .range([height, 0]);

  x.domain([d3.min(all, function(d,i) { return i; }),d3.max(all, function(d,i) { return i; })]);
  y.domain([0, d3.max(all, function(d) { return d.int2; })]);

  var xAxis = d3.svg.axis()
      .scale(x)
      .ticks(all.length)
      .orient("bottom");

  var yAxis = d3.svg.axis()
      .scale(y)
      .orient("left");

  xAxis.tickFormat(function(d, i){
      return i + 1999;
  });
  yAxis.tickFormat(function(d, i){
      return d/1000 + 'k';
  });

  var line = d3.svg.line()
      .x(function(d,i) { return x(i); })
      .y(function(d,i) { return y(d.est); });

  var area = d3.svg.area()
      .x(function(d,i) { return x(i); })
      .y0(function(d) { return y(d.int1); })
      .y1(function(d) {
        return y(d.int2);
      });

  var bisectDate = d3.bisector(function(d) { return d.est; }).left;

  var steps = d3.select('#steps');
  var sections = d3.select('#content');

  var svg = d3.select("#graph").append("svg")
      .attr("width", "100%")
      .attr("height", "100%")
      .attr("viewBox", "0 0 "+(width + margin.left + margin.right)+" "+(height + margin.top + margin.bottom))
      .attr("preserveAspectRatio", "xMidYMid")
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
  
  var defs = svg.append('defs');

  init();

  $('#removeIntro').on('click', function(){
    $('html, body').animate({
      scrollTop: $("."+contents[0].cls+" h3").offset().top - 200
    }, 500);
    setTimeout(function(){
      console.log('clipto');
      $('html, body').animate({
        scrollTop: $("."+contents[0].cls+" h3").offset().top - 200
      }, 0);
    }, 1000);
  });
  $(window).on('scroll', function(){
    if(started){
      removeIntro();
    }else{
      currentDraw();
    }
  });
  function drawAxis(that){
    if(that.about === ''){
      linetip.active = false;
      drawer = d3.selectAll('clippath rect');
    }else{
      linetip.active = true;
      drawer = d3.select('#clip'+that.cls+' rect');
    }

    drawer.transition().duration(2000)
      .attr('width', width);
    
    if(axisDraw.active)
      axisDraw.draw(that);
  }
  function undrawAxis(that){
    d3.select('#clip'+that.cls+' rect')
    .transition().duration(1000)
    .attr('width', 0);
  }
  function allUndraw(){
    var upcls = contents[contents.length - 2].cls;
    $('body').attr('class', upcls);
    d3.selectAll('#steps .step:not(.'+upcls+')').classed('current', false);
    d3.selectAll('clippath:not(#clip'+upcls+') rect')
    .transition().duration(1000)
    .attr('width', 0);    
  }
  function init(){
    createClips();
    createTips();
    beginGraph();
  }
  function createClips(){
    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + (height+50) + ")")
        .call(xAxis);

    svg.append("g")
        .attr("class", "y axis")
        .attr("transform", "translate("+yaxisp+",0)")
        .call(yAxis);

    var shapes = svg.append("g")
        .attr("class", "shape container");

    shapes.append("line")
        .attr("x1", yaxisp)
        .attr("x2", x(all.length))
        .attr("y1", y(mean))
        .attr("y2", y(mean));

    for (var i = 0; i < contents.length; i++) {
      var section = sections.append('section').attr('class', contents[i].cls);
      section.append('h3');
      var slidesUp = section.append('div').attr('class', 'slidesUp');
      slidesUp.append('h4').text(contents[i].title);
      slidesUp.append('p').text(contents[i].quote);

      var step = steps.append('div')
            .attr('class', 'step '+contents[i].cls)
            .style('top', (i*20)+'px');

      step.append('span').text(contents[i].title);

      step.on('click', function(){
        var cls = $(this).attr('class').replace('step ', '');
        $('html, body').animate({
            scrollTop: $("section."+cls+" h3").offset().top - 250
        }, 500);
      });

      var clipid = 'clip'+contents[i].cls;

      defs.append('clipPath')
          .attr('id', clipid)
          .append('rect').attr('height', height)
          .attr('width', 0);

      var pattern = defs.append('pattern')
                      .attr('id', 'pattern'+contents[i].cls)
                      .attr('x', 0).attr('width', 3)
                      .attr('y', 0).attr('height', 3)
                      .attr('patternUnits', 'userSpaceOnUse');
      pattern.append('rect').attr('x', 1).attr('y', 1)
              .attr('width', 2).attr('height', 2);

      var path = shapes.append("path");
      path.datum(contents[i].about)
        .attr("class", "line "+contents[i].cls)
        .attr("clip-path", "url(#"+clipid+")")
        .attr("d", line);

      var aread = shapes.append("path");
      aread.datum(contents[i].about)
        .attr("class", "area "+contents[i].cls)
        .attr("clip-path", "url(#"+clipid+")")
        .attr("d", area)
        .attr("fill", 'url(#pattern'+contents[i].cls+")");
    };
  }
  function createTips(){

    var tips = svg.append('g').attr('class', 'tipper');
    tips.append('g').attr('class', 'hover rects')
        .selectAll('rect').data(all).enter()
        .append('rect').attr('y', 0)
        .attr('height', h)
        .attr('x', function(d,i){
          return x(i);
        }).attr('width', x(1))
        .on('mouseover', function(d,i){
          d3.select('.tipper').classed('active', true);
          var exes = parseInt(d3.select(this).attr('x')) - parseInt(d3.select(this).attr('width'));
          d3.select('.tipper .movers')
            .transition()
            .attr('transform', 'translate('+(exes+0.5)+',0)');
          d3.select('.tipper')
            .selectAll('text.filtered')
            .html(function(dd,ii){
              var texts = '<tspan x="62" dy="1.35em" class="estimate"><tspan x="62" dy="1.35em">Estimated Injury</tspan><tspan dy="1em" x="62" class="est">' + (dd.about[i].est / 1000) + 'K</tspan></tspan><tspan class="low" dy="1.5em" x="13"><tspan>Low: </tspan><tspan>' + (dd.about[i].int1 / 1000) + 'K</tspan></tspan><tspan class="high" x="110"><tspan>High: </tspan><tspan>' + (dd.about[i].int2 / 1000) + 'K</tspan></tspan>';
              return texts;
            });
          if(linetip.active)
            linetip.follow(i)
          else
            linetip.stay();
        })
        .on('mouseleave', function(d,i){
          d3.select('.tipper').classed('active', false);
        });

    var movers = tips.append('g').attr('class', 'movers');

    movers.append('line')
        .attr('y1', h-50)
        .attr('y2', 0)
        .attr('x1', x(1))
        .attr('x2', x(1));


    var moveBoxes = movers.append('g').attr('class', 'boxes')
          .selectAll('g').data(contents).enter()
          .append('g').filter(function(d) { return d.about })
          .attr('class', function(d,i){
            return 'tooltip '+d.cls;
          });

    moveBoxes.on('mouseenter', function(d,i){
          d3.select('.tipper').classed('active', true);
          var cls = d.cls.replace('tooltip ', '');
          if($('body').hasClass('allOfThem')){
            // d3.select('#content section.'+cls).classed('out', false);
            d3.select('body').classed(cls, true);
            d3.select('.step.'+cls).classed('current', true);
          }
        })
        .on('mouseleave', function(d,i){
          var cls = d.cls.replace('tooltip ', '');
          if($('body').hasClass('allOfThem')){
            // if(!d3.select('#content section.'+cls).classed('animated'))
            //   d3.select('#content section.'+cls).classed('out', true);
            d3.select('body').classed(cls, false);
            d3.select('.step.'+cls).classed('current', false);
          }
        });

    moveBoxes.append('rect')
        .filter(function(d) { return d.about })
        .attr('class', function(d,i){
          return d.cls + ' filtered';
        })
        .attr("clip-path", function(d,i){
          var clipid = 'clip'+d.cls;
          return "url(#"+clipid+")";
        })
        .attr('width', x(1.75))
        .attr('height', height / (contents.length * 1.75))
        .attr('x', x(0.125))
        .attr('y', 0);

    moveBoxes.append('rect')
        .attr('class', function(d,i){
          return d.cls + ' label';
        })
        .attr("clip-path", function(d,i){
          var clipid = 'clip'+d.cls;
          return "url(#"+clipid+")";
        })
        .attr('width', x(0.125))
        .attr('height', height / (contents.length * 1.75))
        .attr('x', 0)
        .attr('y', 0);

    moveBoxes.append('text')
        .attr('class', function(d,i){
          return d.cls + ' filtered';
        })
        .attr("clip-path", function(d,i){
          var clipid = 'clip'+d.cls;
          return "url(#"+clipid+")";
        })
        .attr('x', 0)
        .attr('y', 0)
        .html(function(d,i){
          var texts = '<tspan x="62" dy="1.35em" class="estimate"><tspan x="62" dy="1.35em">Estimated Injury</tspan><tspan dy="1em" x="62" class="est">' + (d.about[i].est / 1000) + 'K</tspan></tspan><tspan class="low" dy="1.5em" x="13"><tspan>Low: </tspan><tspan>' + (d.about[i].int1 / 1000) + 'K</tspan></tspan><tspan class="high" x="110"><tspan>High: </tspan><tspan>' + (d.about[i].int2 / 1000) + 'K</tspan></tspan>';
          return texts;
        });
  }
  function beginGraph(){
    $('html, body').animate({
        scrollTop: 0
    }, 50);

    setTimeout(function(){
      $('html, body').animate({
          scrollTop: $("#introSlide").offset().top - 250
      }, 1000);
      setTimeout(function(){
        $("section:first-child").css("margin-top", 0);
        $('html, body').animate({
          scrollTop: $("#introSlide").offset().top - 250
        }, 0);
        started = true;
      }, 1000);
    }, 100);     
  }
  function currentDraw(){

    var curs = $('#content section h3:in-viewport');
    var tops = $('#content section h3:above-the-top');
    var bots = $('#content section h3:below-the-fold');
    for (var j = 0; j < contents.length; j++) {
      viewer(bots, false);
      viewer(tops, false);
      viewer(curs, true);

      function viewer(data, view){
        for (var i = 0; i < data.length; i++) {
          var elm = $(data[i]).closest('section');
          if(view)
            inside(elm);
          else
            out(elm);
        };        
      }
      function out(elm){
        if( elm.hasClass(contents[j].cls) && !elm.hasClass('out') ){
          contents[j].undraw();
          elm.addClass('out').removeClass('animated');
          var cls = elm.attr('class').replace(' out', '');
          $('.step.'+cls).removeClass('current');
          $('body').removeClass(cls);
        }
      }
      function inside(elm){
        if( elm.hasClass(contents[j].cls) && !elm.hasClass('animated') ){
          contents[j].draw();
          elm.addClass('animated').removeClass('out');
          var cls = elm.attr('class').replace(' animated', '');
          $('.step.'+cls).addClass('current');
          $('body').addClass(cls);
        }
      }


    };
  }
  function removeIntro(){
    d3.select('html').classed('introduced', true);
    started = false;
    setTimeout(function(){
      $('html, body').animate({
        scrollTop: $("."+contents[0].cls).offset().top - 250
      }, 0);
    }, 1000);
  }

}); // end of no-conflict